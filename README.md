# Terraform on Localstack

## prerequisites

#### VM enabled OS (unless running Linux)

https://docs.docker.com/docker-for-windows/troubleshoot/#virtualization
#### docker

https://docs.docker.com/get-docker/
#### docker-compose

https://docs.docker.com/compose/install/
#### AWS CLI

https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html
#### Terraform

https://learn.hashicorp.com/tutorials/terraform/install-cli

## Running and testing the infrastructure execution plan

#### run localstack

Open a terminal and run this command in the `localstack` folder
```shell
TMPDIR=/private$TMPDIR docker-compose up
```
(TMPDIR is a workaround for MacOS, see [Localstack's Github](https://github.com/localstack/localstack#troubleshooting))

#### Configure the aws cli, create new bucket & upload files

In a new terminal window, go to the project root folder.
Run aws configure and fill in some mandatory fields. When running Localstack, this does not have to be very strict:
```shell
aws configure
```
filling in values like so:
```shell
AWS Access Key ID [None]: foo
AWS Secret Access Key [None]: bar
Default region name [eu-west-1]:
Default output format [None]:
```
 create an s3 bucket on localstack and upload the files that contain the lambda functions:
```shell
aws --endpoint-url=http://localhost:4566 s3api create-bucket --bucket=example-bucket
aws --endpoint-url=http://localhost:4566 s3 cp lambda.zip s3://example-bucket/v1.0.0/lambda.zip
aws --endpoint-url=http://localhost:4566 s3 cp lambda2.zip s3://example-bucket/v1.0.1/lambda.zip
```
To verify success, you can navigate an s3 bucket with the `ls` command:
```shell
aws --endpoint-url=http://localhost:4566 s3 ls s3://example-bucket/v1.0.0/lambda.zip
```

#### Initiate Terraform, create the execution plan and apply it to the configured Localstack AWS environment

Before applying terraform file, run
```shell
terraform init
```
so terraform can pull the proper provider code. Then, either use `terraform plan` to only view what the resulting state will be, or `terraform apply -var="app_version=1.0.0"` to bring up the infrastructure with the first version of the lambda.

#### View the result

If the apply command succeeded, you can now explore the result with the following:
view the list of lambda's:
```shell
aws --endpoint-url=http://localhost:4566 lambda list-functions
```
make a direct call to the lambda:
```shell
aws --endpoint-url=http://localhost:4566 lambda invoke --function-name ExampleLambda /dev/stdout
```
Visit the base_path outputted by Terraform at Localstack's ip address in a browser, for example:
```
http://localhost:4566/restapis/z54snzignl/test/_user_request_/
```
#### Switching versions

You can update your infrastructure by re-applying the execution plan with a new version: 
```shell
terraform apply -var="app_version=1.0.1"
```
In this case, the lambda service will use the second file you uploaded, located under the s3 bucket's 1.0.1 subfolder, which will show a different page.

#### Cleaning up

If you want to remove the infrastructure itself, simply run `terraform destroy -var="app_version=1.0.0"` to let terraform handle the cleanup of the existing resources.

You can remove an s3 bucket and its contents separately with this command:
```shell
aws --endpoint-url=http://localhost:4566 s3 rb s3://example-bucket --force
```

You can stop the localstack instance by terminating the running shell command in the first terminal, e.g. with `CTRL-C`
